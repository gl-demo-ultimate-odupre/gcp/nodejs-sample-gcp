const request = require('supertest');
const app = require('../app');

describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome to 🚂 Express/, done);
  });
}); 

const { numberA, numberB, numberC } = require('../src/numbers');

describe('numberA', () => {
  test('should return 0 for input 0', () => {
    expect(numberA(0)).toBe(0);
  });

  test('should return correct values for positive inputs', () => {
    expect(numberA(1)).toBe(1);
    expect(numberA(2)).toBe(5);
    expect(numberA(3)).toBe(12);
    expect(numberA(4)).toBe(22);
    expect(numberA(5)).toBe(35);
  });
});

describe('numberB', () => {
  test('should return 2 for input 0', () => {
    expect(numberB(0)).toBe(2);
  });

  test('should return 1 for input 1', () => {
    expect(numberB(1)).toBe(1);
  });

  test('should return correct values for positive inputs', () => {
    expect(numberB(2)).toBe(3);
    expect(numberB(3)).toBe(4);
    expect(numberB(4)).toBe(7);
    expect(numberB(5)).toBe(11);
    expect(numberB(6)).toBe(18);
  });
});

describe('numberC', () => {
  test('should return 0 for input 0', () => {
    expect(numberC(0)).toBe(0);
  });

  test('should return 1 for input 1', () => {
    expect(numberC(1)).toBe(1);
  });

  test('should return correct values for positive inputs', () => {
    expect(numberC(2)).toBe(1);
    expect(numberC(3)).toBe(2);
    expect(numberC(4)).toBe(3);
    expect(numberC(5)).toBe(5);
    expect(numberC(6)).toBe(8);
  });
});