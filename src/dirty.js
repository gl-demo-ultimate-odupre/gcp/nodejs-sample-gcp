var computer = new Object();
    computer.caseColor = 'black';
    computer.brand = 'Dell';
    computer.value = 1200;
    computer.onSale = true;


function greet(name) {
    return `Hi, ${name}`; //template literal
}
console.log(greet('Nice'));

function greetDirty(name) {
    return 'Hi ' + name;
}
console.log(greetDirty('Dirty'));

// This is dirty code
var score = 0;
var scrore = 1;
var scrore = 0;

//Disable the question button
function disableButton(btnID){
        document.getElementById(btnID.id).disabled = true;
    }

//Show current score
function endQuestion(){
  alert ("Your total score is now " +score);
}

//Toggle Images On
function showImgBtn1(){
  document.getElementById('btn1pic').style.visibility = 'visible';  setTimeout(askQuestion1,3000);
}
        function showImgBtn2(){
    document.getElementById('btn2pic').style.visibility = 'visible';  setTimeout(askQuestion2,3000);
                    }
//This keeps going for every question--repeated 9 to 20 times


//Questions
function askQuestion1()
{
  var answer = prompt ("What body system contains the heart, blood, arteries, and veins?") .toLowerCase();
   if (answer==="circulatory") {
    alert ("Correct!");
     score += 100;
  }
  else {
    alert ("Sorry, incorrect.");
     score -= 100;
  }
    endQuestion();
                                    document.getElementById('btn1pic').style.visibility = 'hidden';
}

function askQuestion2()
{
  var answer = prompt ("What body system contains the brain, spinal cord, and nerves?") .toLowerCase();
  if (answer==="nervous") {
   alert ("Correct!");
    score += 200;
  }
  else {
    alert ("Sorry, incorrect.");
    score -= 200;
  }
  endQuestion();
  document.getElementById('btn2pic').style.visibility = 'hidden';
}

// Add a CWE-73

function readUserInput(filename) {
  const fs = require('fs');
  const path = require('path');

  // CWE-73: Improper Neutralization of File Path
  const filePath = path.join(__dirname, filename);
  
  try {
    const content = fs.readFileSync(filePath, 'utf8');
    console.log('File content:', content);
  } catch (error) {
    console.error('Error reading file:', error.message);
  }
}

// Example usage
readUserInput('../user_input.txt');
