// Create a JS animation with a ball bouncing on the borders of a canva, and changing color each time it hits a border
// Get canvas element from DOM
const canvas = document.getElementById("canvas");

// Get 2d drawing context for canvas
const ctx = canvas.getContext("2d");

// Canvas width and height  
const width = canvas.width;
const height = canvas.height;

// Ball radius and color
const ballRadius = 10;
let ballColor = "red";

// Ball position and velocity
let x = width / 2;
let y = height / 2;
let dx = 3;
let dy = 5;

// Draw ball on canvas 
function drawBall() {

  // Set fill color 
  ctx.fillStyle = ballColor;

  // Begin path
  ctx.beginPath();

  // Draw circle with center at (x, y) and radius ballRadius
  ctx.arc(x, y, ballRadius, 0, Math.PI * 2);

  // Fill the path
  ctx.fill();
}

// Returns a random RGB color value
function randomColor() {
  // Call random helper function three times to generate
  // a random number between 0 and 255 for each RGB component
  return `rgb(${random(255)}, ${random(255)}, ${random(255)})`;
}

// Returns a random whole number between 0 and the max number
function random(number) {
  // Use Math.random() and Math.floor() to generate
  // a random whole number between 0 and number
  return Math.floor(Math.random() * number);
}

// Recursively call animate function to create animation loop  
function animate() {

  // Request next animation frame
  requestAnimationFrame(animate);

  // Clear canvas
  ctx.clearRect(0, 0, width, height);

  // Update ball position
  x += dx;
  y += dy;

  // Check if ball hits left or right edge
  if (x + dx > width || x + dx < 0) {

    // Reverse direction 
    dx = -dx;

    // Update position
    x += dx;

    // Generate new color
    ballColor = randomColor();
  }

  // Check if ball hits top or bottom edge
  if (y + dy > height || y + dy < 0) {

    // Reverse direction
    dy = -dy;

    // Update position 
    y += dy;

    // Generate new color
    ballColor = randomColor();
  }

  // Draw ball in updated position
  drawBall();
}

// Start animation
animate();