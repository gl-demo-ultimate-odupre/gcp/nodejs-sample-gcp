// Generate 3 methods to compute the pell, the lukas and the fibonacci numbers, given an index.
// Name these 3 methods numberA, B and C.
// Name all variables with only one letter

function numberA(i) {
  if (i === 0) return 0; 
  else return i * (3 * i - 1) / 2;
}

function numberB(i) {
  if (i === 0) return 2;
  else if (i === 1) return 1;
  else return numberB(i - 1) + numberB(i - 2); 
}

function numberC(i) {
  if (i === 0) return 0;
  else if (i === 1) return 1;
  else return numberC(i - 1) + numberC(i - 2);
}

// Export the three functions for reuse
module.exports = {
  numberA,
  numberB,
  numberC
}

