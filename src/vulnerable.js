// Add a function that creates a vulnerable has of a given string, using sha1. Expose the function so it can be re-used. Document the code you are providing.
const crypto = require('crypto');

/**
 * Creates a vulnerable hash of a given string using SHA1.
 * WARNING: This function is intentionally vulnerable and should not be used for secure hashing.
 * SHA1 is considered cryptographically weak and unsuitable for security-critical applications.
 * 
 * @param {string} input - The string to be hashed.
 * @returns {string} The SHA1 hash of the input string.
 */
function createVulnerableHash(input) {
  return crypto.createHash('sha1').update(input).digest('hex');
}

module.exports = {
  createVulnerableHash
};