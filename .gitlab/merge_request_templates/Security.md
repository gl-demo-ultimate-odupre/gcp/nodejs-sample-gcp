## Description

This draft merge request resolves the issue.

### Changes

The following changes were made:

- [ ] Include the static security templates
  - [ ] template: Jobs/SAST.gitlab-ci.yml
  - [ ] template: Jobs/Container-Scanning.gitlab-ci.yml
  - [ ] template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - [ ] template: Jobs/Secret-Detection.gitlab-ci.yml
- [ ] Include the dynamic security template
  - [ ] template: Security/DAST.gitlab-ci.yml
- [ ] Include the license scanning template
  - [ ] template: Jobs/License-Scanning.gitlab-ci.yml

### Tests

- [ ] Validate that SAST identified threats
  - [ ] Ask AI for an explanation
  - [ ] Automatically generate a fix with AI
- [ ] Validate that Secret detection works as expected
  - [ ] Push a token with the code
  - [ ] Checks that the token has been identified and revoked