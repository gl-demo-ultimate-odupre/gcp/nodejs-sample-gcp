import os
import requests
import time
from datetime import datetime, timedelta

# Get environment variables
API_V4_URL = os.getenv('CI_API_V4_URL')         # Predefined variables
PROJECT_ID = os.getenv('CI_PROJECT_ID')         # Predefined variables
PAT = os.getenv('PAT')                          # A project access token with the Developer role and the api scope
AGE_THRESHOLD = os.getenv('AGE_THRESHOLD')      # An integer representing the number of days old a pipeline must be to be deleted

# Check if env variables are set
if not API_V4_URL:
    raise Exception('CI_API_V4_URL is not set')
if not PROJECT_ID:
    raise Exception('CI_PROJECT_ID is not set')
if not PAT:
    raise Exception('PAT is not set')
if not AGE_THRESHOLD:
    # Default to 90 days if AGE_THRESHOLD is not set
    AGE_THRESHOLD = 90
    print(f"Defaulting to threshold of: '{AGE_THRESHOLD}' days")
else:
    AGE_THRESHOLD = int(AGE_THRESHOLD)

# Create a date object that point AGE_THRESHOLD days ago
older_than_date = datetime.now().date() - timedelta(days=AGE_THRESHOLD)

# GitLab API endpoint for a projects pipelines
api_url = f"{API_V4_URL}/projects/{PROJECT_ID}/pipelines"

# Headers for authentication
headers = {
    'PRIVATE-TOKEN': PAT
}

def delete_pipeline(pipeline_id):
    """Delete a pipeline by ID."""
    delete_url = f"{api_url}/{pipeline_id}"
    response = requests.delete(delete_url, headers=headers)
    if response.status_code == 429:
        # Rate limit exceeded, wait and retry
        print("Rate limit exceeded. Waiting for 60 seconds.")
        time.sleep(60)
        return delete_pipeline(pipeline_id)
    elif response.status_code == 204:
        ## 204 is a Generic success response with no body if successful
        print(f"Deleted pipeline with ID: '{pipeline_id}'")
    response.raise_for_status()
    # return response.status_code == 204

def get_pipelines_page(page=1, retries=5):
    """Get a page of pipelines and return the response"""
    response = requests.get(api_url, headers=headers, params={'page': page, 'per_page': 100})
    if (response.status_code == 429 or response.status_code == 500) and retries:
        # Rate limit exceeded, wait and retry
        print(f"Rate limit or 500 error enountered.  Waiting for 60 seconds then trying again. {retries} left")
        time.sleep(60)
        return get_pipelines(page, retries - 1)
    # Unhandled error details
    response.raise_for_status()

    return response

def get_all_pipelines():
    """Get all the pipelines for a given project ID"""
    pipelines = []
    page = 1
    while True:
        print(f"Retrieving page: '{page}' of Pipelines")
        response = get_pipelines_page(page)
        pipelines.extend(response.json())
        page = response.headers.get('x-next-page')
        if not page:
            return pipelines

def sanitize_pipelines(pipelines):
    """Fix the date format for pipelines"""
    required_pipelines = []

    for pipeline in pipelines:
        pipeline_date = datetime.strptime(pipeline['created_at'], '%Y-%m-%dT%H:%M:%S.%f%z').date()
        if pipeline_date < older_than_date:
            required_pipelines.append(pipeline['id'])

    return required_pipelines

def main():
    page = 1
    print(f"Deleting pipelines older than: '{older_than_date}'")
    pipelines = get_all_pipelines()
    print(f"Retrieved '{len(pipelines)}' pipelines")
    pipelines = sanitize_pipelines(pipelines)
    print(f"Would delete '{len(pipelines)}' pipelines")

    for pipeline in pipelines:
        delete_pipeline(pipeline)

if __name__ == "__main__":
    main()
