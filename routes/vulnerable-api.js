// Create a weak API that requires a String and returns its hash based on my existing code. Document the code and expose that API.
const express = require('express');
const router = express.Router();
const { createVulnerableHash } = require('../src/vulnerable');
const fs = require('fs');
const path = require('path');

/**
 * @swagger
 * /api/hash:
 *   post:
 *     summary: Generate a vulnerable hash for a given string
 *     description: This endpoint creates a SHA1 hash of the provided string. WARNING: This is intentionally vulnerable and should not be used for secure applications.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               input:
 *                 type: string
 *     responses:
 *       200:
 *         description: Successful response with the generated hash
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 hash:
 *                   type: string
 */
router.post('/', (req, res) => {
  const { input } = req.body;
  
  if (!input || typeof input !== 'string') {
    return res.status(400).json({ error: 'Invalid input. Please provide a string.' });
  }

  const hash = createVulnerableHash(input);
  res.json({ hash });
});

/**
 * @swagger
 * /api/read-file:
 *   get:
 *     summary: Read a file based on user input (Vulnerable)
 *     description: This endpoint reads a file based on user-provided input. WARNING: This is intentionally vulnerable to path traversal attacks and should not be used in production.
 *     parameters:
 *       - in: query
 *         name: filename
 *         required: true
 *         schema:
 *           type: string
 *         description: The name of the file to read
 *     responses:
 *       200:
 *         description: Successful response with the file contents
 *         content:
 *           text/plain:
 *             schema:
 *               type: string
 *       400:
 *         description: Bad request if filename is not provided
 *       500:
 *         description: Internal server error if file reading fails
 */
router.get('/read-file', (req, res) => {
  const { filename } = req.query;

  if (!filename) {
    return res.status(400).json({ error: 'Filename is required' });
  }

  // CWE-73: Improper Neutralization of File Path
  const filePath = path.join(__dirname, filename);

  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Error reading file:', err);
      return res.status(500).json({ error: 'Failed to read file' });
    }
    res.send(data);
  });
});

// Add CWE-116

/**
 * @swagger
 * /api/improper-output:
 *   post:
 *     summary: Demonstrate improper output encoding (CWE-116)
 *     description: This endpoint takes user input and returns it without proper encoding. WARNING: This is intentionally vulnerable and should not be used in production.
 *               userInput:
 *                 type: string
 *     responses:
 *       200:
 *         description: Successful response with the unencoded user input
 *                 output:
 *                   type: string
 */
router.post('/improper-output', (req, res) => {
  const { userInput } = req.body;

  if (!userInput || typeof userInput !== 'string') {
    return res.status(400).json({ error: 'Invalid input. Please provide a string.' });
  }

  // CWE-116: Improper Output Encoding or Escaping
  // This is intentionally vulnerable. Do not use in production.
  const output = `<div>${userInput}</div>`;

  res.json({ output });
});


// Add CWE-369

/**
 * @swagger
 * /api/divide:
 *   post:
 *     summary: Demonstrate division by zero vulnerability (CWE-369)
 *     description: This endpoint performs division based on user input. WARNING: This is intentionally vulnerable to division by zero and should not be used in production.
 *               numerator:
 *                 type: number
 *               denominator:
 *                 type: number
 *     responses:
 *       200:
 *         description: Successful response with the division result
 *                 result:
 *                   type: number
 *       400:
 *         description: Bad request if input is invalid
 *       500:
 *         description: Internal server error if division by zero occurs
 */
router.post('/divide', (req, res) => {
  const { numerator, denominator } = req.body;

  if (typeof numerator !== 'number' || typeof denominator !== 'number') {
    return res.status(400).json({ error: 'Invalid input. Please provide numeric values.' });
  }

  // CWE-369: Divide By Zero
  const result = numerator / denominator;

  res.json({ result });
});



module.exports = router;
